/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Negocio.BigLong;

/**
 *
 * @author madar
 */
public class Test_BigLong {
    public static void main(String[] args) {
        String num1="999999999999999999999999999999";
        String num2="1";
        BigLong n1=new BigLong(num1);
        BigLong n2=new BigLong(num2);
        System.out.println("Suma es: "+n1.getSuma(n2).toString());
        //1000000000000000000000000000000
        System.out.println("Resta es: "+n1.getResta(n2).toString());
        //999999999999999999999999999998
        System.out.println("Resta es: "+n2.getResta(n1).toString());
        //-999999999999999999999999999998
    }
    
}
