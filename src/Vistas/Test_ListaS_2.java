/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import java.util.Random;
import java.util.Scanner;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class Test_ListaS_2 {

    public static void main(String[] args) {
        ListaS<Integer> numeros = new ListaS();
        int cant = leerInt("Digite cuantos nodos desea:");
        Random n = new Random();
        while (cant-- > 0) {
            numeros.insertarFin(n.nextInt(10));
        }
        System.out.println("Mi lista es:" + numeros.toString());

        int sum = 0;
        for (int i = 0; i < numeros.getTamanio(); i++) {
            sum += numeros.get(i);
        }
        System.out.println("La suma es:" + sum);

    }

    private static int leerInt(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Entero");
            return leerInt(msg);
        }

    }

}
