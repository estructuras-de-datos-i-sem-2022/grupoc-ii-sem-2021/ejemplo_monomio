/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Monomio;
import Negocio.Secuencia;

/**
 *
 * @author madarme
 */
public class Test_Secuencia_Generica {

    public static void main(String[] args) {
        Secuencia<Integer> s = new Secuencia(3);
        Secuencia<String> s2 = new Secuencia(3);
        Secuencia<Monomio> s3 = new Secuencia(3);

        s.adicionar(0, 1333);
        s.adicionar(1, 31);
        s.adicionar(2, 333);

        s2.adicionar(0, "Madarme");
        s2.adicionar(1, "Pepe");
        s2.adicionar(2, "Edna");

        s3.adicionar(0, new Monomio(3, 'x', 4));
        s3.adicionar(1, new Monomio(13, 'z', 2));
        s3.adicionar(2, new Monomio(43, 'w', 1));
        
        s.ordenar_Burbuja();
        s2.ordenar_Burbuja();
        s3.ordenar_Burbuja();
        
        imprimirSecuencia(s);
        imprimirSecuencia(s2);
        imprimirSecuencia(s3);

    }

    private static <T> void imprimirSecuencia(Secuencia s) {
        String msg = "";
        for (int i = 0; i < s.length(); i++) {

            msg += s.getElemento(i).toString() + "\t";
        }
        System.out.println(msg);
    }
}
