/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Vaca;
import ufps.util.colecciones_seed.ListaS;

/**
 * Clase para probar los métodos de una lista simple enlazada
 *
 * @author madarme
 */
public class Test_ListaS {

    public static void main(String[] args) {
        ListaS<String> l1 = new ListaS();
        ListaS<Integer> l2 = new ListaS();
        ListaS<Vaca> potrero = new ListaS();

        /*
            Crear vacas
         */
        potrero.insertarInicio(new Vaca("Dulcinea", 200F));
        potrero.insertarInicio(new Vaca("Pepa", 300F));
        potrero.insertarInicio(new Vaca("Lucinda", 100F));

        /*
            Imprime potrero.toString:
            Cabeza-->
                    (lucinda, 100)-(pepa,300)-(dulcinea,200)-->NULL
         */
        System.out.println("Mi lista es:\n" + potrero.toString());

        /*Vaca vector[] = potrero.toArray2();
        System.out.println("Esto es la lista en un vector:");
        for (Vaca v : vector) {
            System.out.println("Vaca:" + v.toString());
        }*/
        
        Object vectorVaca[] = potrero.toArray2();
        for (Object vaca : vectorVaca) {
            Vaca mia = (Vaca) vaca;
            System.out.println(mia.toString());
        }

        l1.insertarInicio("Luis");
        l1.insertarInicio("Anderson");
        l1.insertarInicio("Edna");
        l1.insertarInicio("Jaider");

        System.out.println("Mi lista es:\n" + l1.toString());
        /*
            Imprimo l1:
            Jaider-Edna-Anderson-Luis
         */
        l1.insertarFin("Madarme");
        l1.insertarFin("Jenifer");
        l1.insertarFin("Juan P");

        /*
            Imprimo L1:
            cabeza-->
                    Jaider-Edna-Anderson-Luis-Madarme-Jenifer-Juan P --> NULL
        
         */
        System.out.println("El tamaño de L1 es:" + l1.getTamanio());

        l1.insertarInicio("luis");
        l1.insertarInicio("anderson"); //l1.gettamanio= 2

    }

}
