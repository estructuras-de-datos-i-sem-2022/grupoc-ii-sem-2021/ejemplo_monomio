/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madarme
 */
public class Bit {

    //true--> 1, false-->0
    private boolean valor;

    public Bit(boolean valor) {
        this.valor = valor;
    }

    public Bit() {
    }

    public boolean isValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {

        String dato = this.valor ? "1" : "0";
        return dato;
    }

}
